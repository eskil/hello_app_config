# Hello App Gitops Config

# Setup

## Minikube

Install minikube on your mac

```shell
❯ brew install minikube
```


Check minikube

```shell
❯ minikube status
minikube
type: Control Plane
host: Stopped
kubelet: Stopped
apiserver: Stopped
kubeconfig: Stopped
```

### Multi cluster

Start two clusters, a `dev` where we'll run argo and test deploys, and
a `deploy` for deployed. We're not calling it `prod` to avoid
conflicts with anything you're actually working with.

If you just want to run one cluster, replace all `minikube-dev` and
`minikube-deploy` here with just `minikube`.

```shell
❯ minikube start -p minikube-dev --static-ip=192.168.85.2

# ... 10 lines of emoji heave stuff
🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default

❯ minikube status
minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured


❯ minikube start -p minikube-deploy --static-ip=192.168.95.2
...
```

NOTE: 
* you'll need to rerun `minikube start` after restarts of your laptop/docker**
* you cannot just stop/start the minikube-xyz in docker, it'll leave the old ports behind in `~/.kube/config`
* use `minikube stop -p minikube-xyz` and `start` to restart.

### Cleanup

Should you want to start over;
```shell
❯ minikube delete -p minikube-deploy
🔥  Deleting "minikube-deploy" in docker ...
🔥  Deleting container "minikube-deploy" ...
🔥  Removing /Users/eskil.olsen/.minikube/machines/minikube-deploy ...
💀  Removed all traces of the "minikube-deploy" cluster.
❯ minikube delete -p minikube-dev
🔥  Deleting "minikube-deploy" in docker ...
🔥  Deleting container "minikube-deploy" ...
🔥  Removing /Users/eskil.olsen/.minikube/machines/minikube-deploy ...
💀  Removed all traces of the "minikube-deploy" cluster.
```

### Switch to context

Switch to `minikube-dev` cluster.

```shell
❯ kubectl config use-context minikube-dev

❯ kubectl cluster-info
Kubernetes control plane is running at https://127.0.0.1:50357
CoreDNS is running at https://127.0.0.1:50357/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

❯ kubectl config current-context
minikube-dev
```


### Ingress

WIP: DON'T DO THIS

We add the ingress plugin to get nginx ingress later. 
See also: https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/


```shell
minikube addons enable ingress -p minikube-dev
minikube addons enable ingress -p minikube-deploy
```


## Argo

### Install into minikube

See also: [Installing ArgoCD on Minikube and deploying a test application](https://medium.com/@mehmetodabashi/installing-argocd-on-minikube-and-deploying-a-test-application-caa68ec55fbf)


```shell
# Create a namespace for argocd
❯ kubectl create ns argocd

# Apply the installation, note the version number, pick whichever recent you want https://github.com/argoproj/argo-cd/tags
❯ kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/v2.10.7/manifests/install.yaml
customresourcedefinition.apiextensions.k8s.io/applications.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/applicationsets.argoproj.io created
customresourcedefinition.apiextensions.k8s.io/appprojects.argoproj.io created
serviceaccount/argocd-application-controller created
...

# Check what's setup in the namespace
❯ kubectl get all -n argocd
... bunch of stuff
```

### Login

Once running (see previous section), forward port 34800 to argo's UI; 

**NOTE: this might not work immediately after installing ArgoCD, wait a bit**

```shell
❯ kubectl port-forward svc/argocd-server -n argocd 34800:443

# If ArgoCD hasn't started yet...
error: unable to forward port because pod is not running. Current status=Pending 

# If argo works
Forwarding from 127.0.0.1:34800 -> 8080
Forwarding from [::1]:34800 -> 8080
```

**NOTE: leave running in a terminal for accessing the web _later_**

Now login as admin and change the password. Get argocd initial `admin` password; 
```shell
❯ argocd admin initial-password -n argocd
<password>

 This password must be only used for first time login. We strongly recommend you update the password using `argocd account update-password`.
```

To change the password, you have to login first.

```shell
❯ argocd login localhost:34800
WARNING: server certificate had error: tls: failed to verify certificate: x509: certificate signed by unknown authority. Proceed insecurely (y/n)? y
Username: admin
Password: <password from above>
'admin:login' logged in successfully
Context 'localhost:34800' updated
```

If you didn't login, you'll get this error.
```
FATA[0007] rpc error: code = Unauthenticated desc = invalid session: signature is invalid
```

Or change password
```shell
❯ argocd account update-password --account admin
*** Enter password of currently logged in user (admin): <password from above>
*** Enter new password for user admin: <new password>
*** Confirm new password for user admin: <new password>
Password updated
Context 'localhost:34800' updated
```

open https://localhost:34800 and login as admin/new password.

### Setup clusters

So far argo comes up with 1 default cluster;

```shell
❯ argocd cluster list
SERVER                          NAME        VERSION  STATUS   MESSAGE                                                  PROJECT
https://kubernetes.default.svc  in-cluster           Unknown  Cluster has no applications and is not being monitored.
```

We need to rename this to `minikube-dev` and add `minikube-deploy`

```shell
❯ argocd cluster set in-cluster --name minikube-dev
Cluster 'in-cluster' updated.
```

**NOTE: stop here for this section, I cannot make cluster add work**

```shell
❯ argocd cluster add minikube-deploy --core --yes --insecure
WARNING: This will create a service account `argocd-manager` on the cluster referenced by context `minikube-deploy` with full cluster level privileges. Do you want to continue [y/N]? y
INFO[0000] ServiceAccount "argocd-manager" already exists in namespace "kube-system"
INFO[0000] ClusterRole "argocd-manager-role" updated
INFO[0000] ClusterRoleBinding "argocd-manager-role-binding" updated
FATA[0000] rpc error: code = Unknown desc = Get "https://127.0.0.1:54103/version?timeout=32s": dial tcp 127.0.0.1:54103: connect: connection refused
```

All of this fails, I have no idea how to make argocd-server access the
k8s on the other cluster. Attempts at mucking with ports in
`~/.kube/config` and change `ClusterIP` to `NodePort` all
failed. Using `--core` fails with
```
FATA[0001] error retrieving argocd-cm: configmap "argocd-cm" not found
```

So we'll just continue with a single cluster :-/

Go back to https://www.youtube.com/watch?v=s_o8dwzRlu4 1hr 8min for IP/port info or try [kind](https://kind.sigs.k8s.io/)

## Hello App

Hello App is our dumb little webapp we'll setup and deploy; https://gitlab.com/eskil/hello-app

### Projects

We'll create a `hello-project` project for the hello-app to live in
```shell
❯ argocd proj create hello-project
❯ argocd proj add-destination hello-project "*" "*"
❯ argocd proj allow-cluster-resource hello-project "*" "*"
❯ argocd proj add-source hello-project https://gitlab.com/eskil/hello-app-config.git
```

TODO: there's some exploring to do about destinations/clusters...


### Gitlab Docker Registry

Our initial service deploy needs a container to pull. Build and push that to gitlab.

```shell
❯ git clone https://gitlab.com/eskil/hello-app.git

❯ cd hello-app.git

# Login, possibly using a token rather than password
❯ docker login registry.gitlab.com 

# Build image
❯ docker build --tag registry.gitlab.com/eskil/hello-app:0.1.0 .

# Push to registry
❯ docker push registry.gitlab.com/eskil/hello-app:0.1.0
```

### Deploy

We're going to do two versions, first an argo `application`, then an `application set` that uses the cluster generator. This is just for education.

#### Application

The `application` is to test drive simple deploys with fairly
cut'n'pasted k8s setup (from k8s page and random instructional videos
like (this)[https://www.youtube.com/watch?v=s_o8dwzRlu4]

* It's argo setup is defined in `application.yaml`
* which defines it as a `applicaton` named `hello-app-dev` in the `argocd` namespace (how it'll appear in the UI)
* it's `source` (deployment config) is this repo's `dev/` path
* it's `destination` is the `hello-app` namespace 

**NOTE: it does not use your local checkout version! the `source` specifically says to checkout this config repo and use the `dev/` there. Declarative vs. Imperative**

In the `dev` setup we define a k8s application fairly manually

* configmap and secret (needed by Phoenix in the app)
* deployment that specifies the image and image tag
  * env settings that refer to configmap and secret (`configmap.yaml` and `secret.yaml`)
  * service that sets up relevant ports

Apply the `hello_app` application  setup from this repo

```shell
❯ kubectl apply -f application.yaml
application.argoproj.io/hello-app-dev created
```

#### Access Application

We need to us minikube to tunnel to the service;

```shell
❯ minikube service hello-app-dev-service  -n hello-app -p minikube-dev
|-----------|-----------------------|-------------|---------------------------|
| NAMESPACE |         NAME          | TARGET PORT |            URL            |
|-----------|-----------------------|-------------|---------------------------|
| hello-app | hello-app-dev-service |        4000 | http://192.168.85.2:30400 |
|-----------|-----------------------|-------------|---------------------------|
🏃  Starting tunnel for service hello-app-dev-service.
|-----------|-----------------------|-------------|------------------------|
| NAMESPACE |         NAME          | TARGET PORT |          URL           |
|-----------|-----------------------|-------------|------------------------|
| hello-app | hello-app-dev-service |             | http://127.0.0.1:63436 |
|-----------|-----------------------|-------------|------------------------|
🎉  Opening service hello-app/hello-app-dev-service in default browser...
❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it.
```

This will open http://127.0.0.1:<temp> port in your default browser.

#### ApplicationSet

Apply the `hello_app` application SET setup from this repo

```shell
❯ kubectl apply -f applicationset.yaml
applicationset.argoproj.io/hello-app created
```

Once everything is running, you can futz around with the cli

```shell
❯ argocd app get argocd/minikube-dev-hello-app
Name:               argocd/minikube-dev-hello-app
Project:            hello-project
Server:             https://kubernetes.default.svc
Namespace:          hello
URL:                https://localhost:34800/applications/minikube-dev-hello-app
Repo:               https://gitlab.com/eskil/hello-app-config.git
Target:             HEAD
Path:               helm/hello-app
Helm Values:        values/minikube-dev.yaml
SyncWindow:         Sync Allowed
Sync Policy:        Automated (Prune)
Sync Status:        Synced to HEAD
Health Status:      Healthy

GROUP  KIND            NAMESPACE  NAME       STATUS   HEALTH   HOOK  MESSAGE
       Namespace                  hello      Running  Synced         namespace/hello created
       ServiceAccount  hello      hello-app  Synced                  serviceaccount/hello-app created
       Service         hello      hello-app  Synced   Healthy        service/hello-app created
apps   Deployment      hello      hello-app  Synced   Healthy        deployment.apps/hello-app created
```

#### Access the app

```shell
❯ minikube service hello-app -n hello -p minikube-dev
|-----------|-----------|-------------|--------------|
| NAMESPACE |   NAME    | TARGET PORT |     URL      |
|-----------|-----------|-------------|--------------|
| hello     | hello-app |             | No node port |
|-----------|-----------|-------------|--------------|
😿  service hello/hello-app has no node port
🏃  Starting tunnel for service hello-app.
|-----------|-----------|-------------|------------------------|
| NAMESPACE |   NAME    | TARGET PORT |          URL           |
|-----------|-----------|-------------|------------------------|
| hello     | hello-app |             | http://127.0.0.1:55535 |
|-----------|-----------|-------------|------------------------|
🎉  Opening service hello/hello-app in default browser...
❗  Because you are using a Docker driver on darwin, the terminal needs to be open to run it.
```
